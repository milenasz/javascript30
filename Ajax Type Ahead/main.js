class AjaxTypeAhead {

    constructor() {

        this.endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';
        this.cities = [];
        this.searchInput = document.querySelector('.search');
        this.suggestions = document.querySelector('.suggestions');

        this.connectApi();
        
    }

    findMatches(value) {
        return this.cities.filter(place => {
            const regex = new RegExp(value, 'gi');
            return place.city.match(regex) || place.state.match(regex)
        });
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    displayMatches(e) {
        const value = e.target.value;
        const matchArray = this.findMatches(value);
        const html = matchArray.map(place => {
            const regex = new RegExp(value, 'gi');
            const cityName = place.city.replace(regex, `<span class="hl">${value}</span>`);
            const stateName = place.state.replace(regex, `<span class="hl">${value}</span>`);
            return `
            <li>
              <span class="name">${cityName}, ${stateName}</span>
              <span class="population">${this.numberWithCommas(place.population)}</span>
            </li>
          `;
        }).join('');
        this.suggestions.innerHTML = html;
    }

    addListenerSearchInput() {
        this.searchInput.addEventListener('change', this.displayMatches.bind(this));
        this.searchInput.addEventListener('keyup', this.displayMatches.bind(this));
    }

    connectApi() {
        fetch(this.endpoint)
            .then(blob => blob.json())
            .then(data => {
                this.cities.push(...data);
                this.addListenerSearchInput();
            })
    }
}

new AjaxTypeAhead();