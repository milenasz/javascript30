class Clock {

    constructor() {
        this.secondHand = document.querySelector('.second-hand');
        this.minsHand = document.querySelector('.min-hand');
        this.hourHand = document.querySelector('.hour-hand');
        this.setDate();
        setInterval(this.setDate.bind(this), 1000);
    }

    setSec(now) {
        const seconds = now.getSeconds();
        const secondsDegrees = ((seconds / 60) * 360) + 90;
        this.secondHand.style.transform = `rotate(${secondsDegrees}deg)`;
    }

    setMin(now) {
        const mins = now.getMinutes();
        const minsDegrees = ((mins / 60) * 360) + ((mins / 60) * 6) + 90;
        this.minsHand.style.transform = `rotate(${minsDegrees}deg)`;
    }

    setHour(now) {
        const hour = now.getHours();
        const hourDegrees = ((hour / 12) * 360) + ((hour / 60) * 30) + 90;
        this.hourHand.style.transform = `rotate(${hourDegrees}deg)`;
    }

    setDate() {
        const now = new Date();

        this.setSec(now);
        this.setMin(now);
        this.setHour(now);
    }
}

new Clock();

