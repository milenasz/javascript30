class MouseMoveShadow {
    constructor() {
        this.wrapper = document.querySelector('.wrapper');
        this.text = document.querySelector('.wrapper h1');
        this.walk = 0.1;
        this.addListeners();
    }

    shadow(e) {
      const {
        offsetLeft: left,
        offsetTop: top,
      } = this.wrapper;
      const { screenX: x, screenY: y } = e;
    
      const xWalk = Math.round(((x - left) * this.walk));
      const yWalk = Math.round(((y - top) * this.walk));
    
      this.text.style.textShadow = `
        ${xWalk}px ${yWalk}px 0 #333,
        ${xWalk * -1}px ${yWalk}px 0 rgba(0,255,255,0.7),
        ${yWalk}px ${xWalk * -1}px 0 rgba(0,255,0,0.7),
        ${yWalk * -1}px ${xWalk}px 0 rgba(0,0,255,0.7)
      `;
    }

    addListeners() {
        window.addEventListener('mousemove', this.shadow.bind(this));
    }
}

new MouseMoveShadow();
