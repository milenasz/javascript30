class DragGallery {
  constructor() {
    this.slider = document.querySelector('.items');
    this.isDown = false;
    this.startX;
    this.scrollLeft;

    this.addListeners();
  }

  deactivateGallery() {
        this.isDown = false;
        this.slider.classList.remove('active');
      }

  mouseDownListener(e) {
    this.isDown = true;
      this.slider.classList.add('active');
      this.startX = e.pageX - this.slider.offsetLeft; 
      this.scrollLeft = this.slider.scrollLeft;
  }

  mouseMoveListener(e) {
    if (!this.isDown) return; 
    e.preventDefault(); 
    const x = e.pageX - this.slider.offsetLeft; 
    const walk = (x - this.startX) * 3; 
    this.slider.scrollLeft = this.scrollLeft - walk;
  }

  addListeners() {
    this.slider.addEventListener('mousedown', (e) => this.mouseDownListener(e));
    this.slider.addEventListener('mouseleave', () => this.deactivateGallery());
    this.slider.addEventListener('mouseup', () => this.deactivateGallery());
    this.slider.addEventListener('mousemove', (e) => this.mouseMoveListener(e));
  }

}

new DragGallery();
