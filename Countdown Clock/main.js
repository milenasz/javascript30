class CountDownTimer {

  constructor() {
    this.countdown = null;
    this.timerDisplay = document.querySelector('.display__time-left');
    this.endTime = document.querySelector('.display__end-time');
    this.buttons = document.querySelectorAll('[data-time]');

    this.init();
  }

  timer(seconds) {
    clearInterval(this.countdown);

    const now = Date.now(); 
    const then = now + seconds * 1000 
    this.displayTimeLeft(seconds);
    this.displayEndTime(then);

    this.countdown = setInterval(() => {
      const secondsLeft = Math.round((then - Date.now()) / 1000);
      if (secondsLeft < 0) {
        clearInterval(this.countdown);
        return;
      }
      this.displayTimeLeft(secondsLeft);
    }, 1000)
  }

  displayTimeLeft(seconds) {
    const minutes = Math.floor(seconds / 60);
    const remainderSeconds = seconds % 60;
    const display = `${minutes}:${remainderSeconds < 10 ? '0' : ''}${remainderSeconds}`;
    document.title = display;
    this.timerDisplay.textContent = display;
  }

   displayEndTime(timestamp) {
    const end = new Date(timestamp);
    const hour = end.getHours();
    const adjustedHour = hour > 12 ? hour - 12 : hour;
    const minutes = end.getMinutes();
    this.endTime.textContent = `Be Back At ${adjustedHour}:${minutes < 10 ? '0' : ''}${minutes}`;
  }
  
   startTimer(button) {
    const seconds = parseInt(button.dataset.time);
    this.timer(seconds);
  }

  userTimer(e) {
    e.preventDefault();
    const mins = document.customForm.minutes.value;
    this.timer(mins * 60);
    document.customForm.reset();
  }
  
  init() {
    this.buttons.forEach(button => button.addEventListener('click', this.startTimer.bind(this, button)));
    document.customForm.addEventListener('submit', e => this.userTimer(e));
  }
}

new CountDownTimer();
