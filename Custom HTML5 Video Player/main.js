class VideoPlayer {

    constructor() {
        this.player = document.querySelector('.player');
        this.video = this.player.querySelector('.viewer');
        this.toggle = this.player.querySelector('.player-button');
        this.ranges = this.player.querySelectorAll('input[type="range"]');
        this.skipButtons = this.player.querySelectorAll('[data-skip]');
        this.progress = this.player.querySelector('.progress');
        this.progressBar = this.player.querySelector('.progress-filled');
        this.btnFullScreen = this.player.querySelector('.player-fullscreen');
        this.mouseDown = false;
        
        this.addListenersBtns();
        this.addListenersProgressBar();
        this.addListenersVideo();
    }

    togglePlay() {
        const method = this.video.paused ? 'play' : 'pause';
        this.video[method]();
    }

    updateButton() {
        const icon = this.video.paused ? '▶' : '❚❚';
        this.toggle.textContent = icon;
    }

    skip(button) {
        this.video.currentTime += parseFloat(button.toElement.dataset.skip);
    }

    handleRangeUpdate(range) {
        this.video[range.target.name] = range.target.value;
    }

    handleProgress() {
        const percent = (this.video.currentTime / this.video.duration) * 100;
        this.progressBar.style.flexBasis = `${percent}%`
    }

    scrub(e) {
        const scrubTime = (e.offsetX / this.progress.offsetWidth) * this.video.duration;
        this.video.currentTime = scrubTime;
    }

    fullScreen() {
        this.video.requestFullscreen();
    }

    addListenersVideo() {
        this.video.addEventListener('click', this.togglePlay.bind(this));
        this.video.addEventListener('play', this.updateButton.bind(this));
        this.video.addEventListener('pause', this.updateButton.bind(this));
        this.video.addEventListener('timeupdate', this.handleProgress.bind(this));
    }

    addListenersBtns() {
        this.toggle.addEventListener('click', this.togglePlay.bind(this));
        this.skipButtons.forEach(button => button.addEventListener('click', button => this.skip(button)));
        this.ranges.forEach(range => range.addEventListener('change', range => this.handleRangeUpdate(range)));
        this.ranges.forEach(range => range.addEventListener('mousemove', range => this.handleRangeUpdate(range)))
        this.btnFullScreen.addEventListener('click', this.fullScreen.bind(this));
    }
    
    addListenersProgressBar() {
        this.progress.addEventListener('click', this.scrub.bind(this));
        this.progress.addEventListener('mousemove', (e) => this.mouseDown && this.scrub(e));
        this.progress.addEventListener('mousedown', () => this.mouseDown = true);
        this.progress.addEventListener('mouseup', () => this.mouseDown = false);
    
    }
}

new VideoPlayer();
