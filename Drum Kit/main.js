class Sound {

    constructor() {
        this.addListenerKeyDown();
        this.addListenerTransitionEnd();
    }

    playSound(e) {
        const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
        const key = document.querySelector(`.key[data-key="${e.keyCode}"]`);
        if (!audio) return;
        audio.currentTime = 0;
        audio.play();
        key.classList.add('playing');
    }
    
    removeTransition(e) {
        const target = e.target;
    
        if (e.propertyName !== 'transform') return;
        target.classList.remove('playing');
    }

    addListenerKeyDown() {
        document.addEventListener('keydown', this.playSound);
    }

    addListenerTransitionEnd() {
        const keys = document.querySelectorAll('.key');
        keys.forEach(key => key.addEventListener('transitionend', this.removeTransition));    
    }
    
}

new Sound();