class Gallery {

    constructor() {
        this.panels = document.querySelectorAll('.panel');
        this.addListenerClick();
        this.addListenerTransitionend();
    }

    toggleOpen() {
        this.classList.toggle('open');
    }

    toggleActive(e) {
        e.propertyName.includes('flex') && this.classList.toggle('open-active');
    }

    addListenerClick() {
        this.panels.forEach(panel => panel.addEventListener('click', this.toggleOpen));
    }

    addListenerTransitionend() {
        this.panels.forEach(panel => panel.addEventListener('transitionend', this.toggleActive));

    }
}

new Gallery();