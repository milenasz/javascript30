class FollowLinks {
  constructor() {
    this.triggers = document.querySelectorAll('a');
    this.highlight = document.createElement('span');
    this.highlight.classList.add('highlight');
    document.body.appendChild(this.highlight);
    this.triggers.forEach(a => a.addEventListener('mouseenter', this.highlightLink.bind(this, a)));

  }

  applyStyles(coords) {
    this.highlight.style.width = `${coords.width}px`;
    this.highlight.style.height = `${coords.height}px`;
    this.highlight.style.transform = `translate(${coords.left}px, ${coords.top}px)`;
  } 

  highlightLink(a) {
    const linkCoords = a.getBoundingClientRect();
    const coords = {
      width: linkCoords.width,
      height: linkCoords.height,
      top: linkCoords.top + window.scrollY,
      left: linkCoords.left + window.scrollX
    };

    this.applyStyles(coords)
  }
}

new FollowLinks();