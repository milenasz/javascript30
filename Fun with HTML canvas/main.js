class DrawCanvas {

    constructor() {
        this.canvas = document.querySelector('#draw');
        this.ctx = this.canvas.getContext('2d');
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        this.ctx.strokeStyle = '#BADA55';
        this.ctx.lineJoin = 'round';
        this.ctx.lineCap = 'round';

        this.isDrawing = false;
        this.lastX = 0;
        this.lastY = 0;
        this.hue = 0;
        this.direction = true;

        this.addListenerMouseDown();
        this.addListenerMouseMove();
        this.addListenerMouseUp();
        this.addListenerMouseOut();
    }

     setLineWidth() {
        (this.ctx.lineWidth >= 30 || this.ctx.lineWidth <= 1) && (this.direction = !this.direction);
        this.direction ? this.ctx.lineWidth++ : this.ctx.lineWidth--
    }
    
     setHUE() {
        this.hue++;
        (this.hue >= 360) && (this.hue = 0);
    }
    
     drawLine(e) {
        this.ctx.strokeStyle = `hsl(${this.hue}, 100%, 50%)`;
        this.ctx.beginPath();
        this.ctx.moveTo(this.lastX, this.lastY);
        this.ctx.lineTo(e.offsetX, e.offsetY);
        this.ctx.stroke();
        [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
    }
    
     draw(e) {
        if (!this.isDrawing) return;
        this.drawLine(e);
        this.setHUE();
        this.setLineWidth();
    }

    addListenerMouseDown() {
        this.canvas.addEventListener('mousedown', (e) => {
            this.isDrawing = true;
            [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
        });
    }
    
    addListenerMouseMove() {
        this.canvas.addEventListener('mousemove', this.draw.bind(this));
    }

    addListenerMouseUp() {
        this.canvas.addEventListener('mouseup', () => this.isDrawing = false);
    }

    addListenerMouseOut() {
        this.canvas.addEventListener('mouseout', () => this.isDrawing = false);
    }

}

new DrawCanvas();