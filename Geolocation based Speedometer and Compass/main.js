class GeolocationAndSpeed {

  constructor () {
    this.arrow = document.querySelector('.arrow');
    this.speed = document.querySelector('.speed-value');
    this.updateData();
  }

  updateData() {
    navigator.geolocation.watchPosition(data => {
      this.speed.textContent = Math.round(data.coords.speed);
      this.arrow.style.transform = `rotate(${data.coords.heading}deg)`;
    }, err => {
      console.error(err);
      alert('Hey! You gotta allow that to happen!!!');
    })
  }
  
}

new GeolocationAndSpeed();
