class CheckedShift {

    constructor() {
        this.checkboxes = document.querySelectorAll('.inbox input[type=checkbox]');
        this.lastChecked = null;
        this.addListenerClick();
    }

    handleCheck(e) {
        e.shiftKey && e.target.checked && this.checkCheckBoxes(e);
        this.lastChecked = e.target;
    }

    checkCheckBoxes(e) {
        let inBetween = false;
        this.checkboxes.forEach(checkbox => {
            (checkbox === e.target || checkbox === this.lastChecked) && (inBetween = !inBetween);
            inBetween && (checkbox.checked = true);
        })
    }

    addListenerClick() {
        this.checkboxes.forEach(checkbox => checkbox.addEventListener('click', this.handleCheck.bind(this)));
    }

}

new CheckedShift();