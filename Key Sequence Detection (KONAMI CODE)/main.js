class Secret {

    constructor() {
        this.proceed = [];
        this.secret = 'unicorn';
        this.input = document.querySelector('#input');
        
        this.addKeyDownListener();
    }

    addKeyDownListener() {
        window.addEventListener('keydown', (e) => {
            const value = e.key;
            this.proceed.push(value);
            const arrCut = this.proceed.slice(-this.secret.length);
          
            this.input.textContent = arrCut.join('-');
            if (arrCut.join('').includes(this.secret)) {
              cornify_add();
              //this.input.textContent = '';
              this.proceed = [];
            }
          });
    }
}

new Secret();


