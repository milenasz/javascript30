class MakeList {
    constructor() {
        this.addItems = document.querySelector('.add-items');
        this.itemsList = document.querySelector('.plates');
        this.items = JSON.parse(localStorage.getItem('items')) || [];
        this.poplateList(this.items, this.itemsList);
        this.addListeners();
    }

    addItem(e) {
        e.preventDefault();
        const text = (document.querySelector('[name=item]')).value;
        const item = {
            text,
            done: false
        }
        this.items.push(item);
        this.poplateList(this.items, this.itemsList);
        localStorage.setItem('items', JSON.stringify(this.items));
        document.getElementById("formular").reset();
    }

    poplateList(plates = [], platesList) {
        platesList.innerHTML = plates.map((plate, i) => {
            return `
                <li>
                    <input type="checkbox" data-index=${i} id="item${i}" ${plate.done ? 'checked' : ''} />
                    <label for="item${i}">${plate.text}</label
                </li>
                `;
        }).join('');
    }

    toggleDone(e) {
        if (!e.target.matches('input')) return;
        const el = e.target;
        const index = el.dataset.index;
        this.items[index].done = !this.items[index].done;
        localStorage.setItem('items', JSON.stringify(this.items));
        this.poplateList(this.items, this.itemsList);
    }


    addListeners() {
        this.addItems.addEventListener('submit', this.addItem.bind(this));
        this.itemsList.addEventListener('click', this.toggleDone.bind(this));
    }
}

new MakeList();
