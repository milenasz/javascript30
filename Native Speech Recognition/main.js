



class SpeechRecording {
  constructor () {
    window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    this.recognition = new SpeechRecognition();
    this.recognition.interimResults = true;
    this.words = document.querySelector('.words');
    this.generateNewLine();
    this.addListeners();
    this.recognition.start();
  }

  secretWord(transcript) {
    if (transcript.includes('time')) {
      console.log('⌚', new Date());
    }
  }

  generateNewLine() {
    this.p = document.createElement('p');
    this.words.appendChild(this.p);
  }

  wroteResults(transcript, e) {
    this.p.textContent = transcript;

    e.results[0].isFinal && this.generateNewLine();

    this.secretWord(transcript);
  }

  transcriptGenerate(e) {
    const transcript = Array.from(e.results)
    .map(result => result[0])
    .map(result => result.transcript)
    .join('');

    this.wroteResults(transcript, e); 
  }

  addListeners() {
    this.recognition.addEventListener('result', e => this.transcriptGenerate(e));
    this.recognition.addEventListener('end',  this.recognition.start)
  }

}

new SpeechRecording();