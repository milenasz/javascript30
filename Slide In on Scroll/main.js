class SlideInImages {
    constructor() {
        this.sliderImages = document.querySelectorAll('.slide-in');
        this.addScrollListener();
    }

    debounce(func , wait = 20, immediate = true) {
        let timeout;
        return function() {
            let contex = this, args = arguments;
            const later = function () {
                timeout = null;
                !immediate && func.apply(contex, args); 
            }
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            callNow && func.apply(contex, args);
        }
    }

    checkSlide(e) {
        this.sliderImages.forEach(sliderImage => {
            const slideInAt = (window.scrollY +window.innerHeight) - sliderImage.height / 2;
            const imageBottom = sliderImage.offsetTop + sliderImage.height;
            const isHalfShown = slideInAt > sliderImage.offsetTop;
            const isNotScrolledPast = window.scrollY < imageBottom;
    
            (isHalfShown && isNotScrolledPast) ? sliderImage.classList.add('active') : sliderImage.classList.remove('active');
        })
    }

    addScrollListener() {
        window.addEventListener('scroll', this.debounce(this.checkSlide.bind(this)));
    }
    
}

new SlideInImages();