class SortArray {
    constructor() {
        this. bands = [
          'The Plot in You',
          'The Devil Wears Prada',
          'Pierce the Veil',
          'Norma Jean',
          'The Bled',
          'Say Anything',
          'The Midway State',
          'We Came as Romans',
          'Counterparts',
          'Oh, Sleeper',
          'A Skylit Drive',
          'Anywhere But Here',
          'An Old Dog',
        ];
        this.publishSortedArray();
    }
    
    strip(str) {
      return str.replace(/(the |a |An |oh, )/i, '').trim();
    }
    
    sortArray (array) {
      return array.sort((a, b) => this.strip(a) > this.strip(b) ? 1 : -1);
    } 

    publishSortedArray() {
      document.querySelector('#bands').innerHTML = this.sortArray(this.bands).map(band => `<li>${band}</li>`).join('');
    }
}

new SortArray();
