class VoiceSimulator {
  constructor() {
    this.msg = new SpeechSynthesisUtterance();
    this.voices = []; 
    this.voicesDropdown = document.querySelector('[name="voice"]');
    this.options = document.querySelectorAll('[type="range"], [name="text"]');
    this.speakButton = document.querySelector('#speak'); 
    this.stopButton = document.querySelector('#stop');
    this.msg.text = document.querySelector('[name="text"]').value;

    this.setVoiceList();
    this.setOptionsButtons();
    this.setDropDownList();
  }

  setVoiceList() {
    speechSynthesis.addEventListener('voiceschanged', this.populateVoices.bind(this));
  }

  setOptionsButtons() {
    this.options.forEach(option => option.addEventListener('change', this.setOption.bind(this, option)));
    this.speakButton.addEventListener('click', this.toggle.bind(this));
    this.stopButton.addEventListener('click', () => this.toggle(false));
  }

  setDropDownList() {
    this.voicesDropdown.addEventListener('change', this.setVoice.bind(this));
  }

  populateVoices() {
    this.voices = speechSynthesis.getVoices();
    this.voicesDropdown.innerHTML = this.voices
      .filter(voice => voice.lang.includes('en'))
      .map(voice => `<option value="${voice.name}">${voice.name} (${voice.lang})`)
      .join('');
  }
  
  setVoice() {
    this.msg.voice = this.voices.find(voice => voice.name === this.voicesDropdown.value);
    this.toggle();
  }
  
  toggle(startOver = true) {
    speechSynthesis.cancel();
    startOver && speechSynthesis.speak(this.msg);
  }
  
  setOption(option) {
    this.msg[option.name] = option.value;
    this.toggle();
  }
}

new VoiceSimulator();
