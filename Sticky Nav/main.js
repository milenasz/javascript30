class StickyNav {

  constructor() {
    this.nav = document.querySelector('#main');
    this.topOfNav = this.nav.offsetTop;
    window.addEventListener('scroll', this.fixNav.bind(this));
  }

  navFixed() {
    document.body.style.paddingTop = this.nav.offsetTop + 'px';
    document.body.classList.add('fixed-nav');
  }

  navNotFixed() {
    document.body.style.paddingTop = 0;
    document.body.classList.remove('fixed-nav');
  }

  fixNav() {
    window.scrollY >= this.topOfNav ? this.navFixed() : this.navNotFixed()
  }

}

new StickyNav();
