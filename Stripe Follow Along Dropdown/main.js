class NavigationBar {

  constructor() {
    this.triggers = document.querySelectorAll('.cool > li');
    this.background = document.querySelector('.dropdownBackground');

    this.addListeners();
  }

  applyStyles(coords) {
    this.background.style.width = `${coords.width}px`;
    this.background.style.height = `${coords.height}px`;
    this.background.style.transform = `translate(${coords.left}px, ${coords.top}px)`;
  }

  applyClasses(trigger) {
    trigger.classList.add('trigger-enter');
    setTimeout(() => trigger.classList.contains('trigger-enter') && trigger.classList.add('trigger-enter-active'), 150);
    this.background.classList.add('open');
  }

  setCoords(trigger) {
    const dropdown = trigger.querySelector('.dropdown');
    const navbar = document.querySelector('.top');
    const dropdownCoords = dropdown.getBoundingClientRect();
    const navCoords = navbar.getBoundingClientRect();

    const coords = {
      height: dropdownCoords.height,
      width: dropdownCoords.width,
      top: dropdownCoords.top - navCoords.top,
      left: dropdownCoords.left - navCoords.left
    }

    return coords;
  }

  handleEnter(trigger) {
    this.applyClasses(trigger);
    
    const coords = this.setCoords(trigger);

    this.applyStyles(coords);
  }

  handleLeave(trigger) {
    trigger.classList.remove('trigger-enter', 'trigger-enter-active');
    this.background.classList.remove('open');
  }

  addListeners() {
    this.triggers.forEach(trigger => trigger.addEventListener('mouseenter', this.handleEnter.bind(this, trigger)));
    this.triggers.forEach(trigger => trigger.addEventListener('mouseleave', this.handleLeave.bind(this, trigger)));
  }

}

new NavigationBar();
