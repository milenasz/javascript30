class UnrealVideo {
  constructor() {
    this.strip = document.querySelector('.strip');
    this.audioSnap = document.querySelector('.snap');
    this.video = document.querySelector('.player');
    this.canvas = document.querySelector('.photo');
    this.ctx = this.canvas.getContext('2d');
    this.photoBtn = document.querySelector('#photo');
    this.greenBtn = document.querySelector('#green');
    this.redBtn = document.querySelector('#red');

    this.getVideo();
    this.addListeners();

  }

  getVideo() {
    navigator.mediaDevices.getUserMedia({
      video: true,
      audioL: false,
    }).then((localMediaStream) => {
      this.video.srcObject = localMediaStream;
      this.video.play();
    }).catch(err => console.error('ERROR: ', err));
  }

  paintToCanavas() {
    const width = this.video.videoWidth;
    const height = this.video.videoHeight;
    this.canvas.width = width;
    this.canvas.height = height;
  
    return setInterval(() => {
      this.ctx.drawImage(this.video, 0, 0, width, height);
      const pixels = this.ctx.getImageData(0, 0, width, height);
      this.ctx.putImageData(this.greenScreen(pixels), 0, 0);
    }, 20);
  }

  paintToCanavas() {
    const width = this.video.videoWidth;
    const height = this.video.videoHeight;
    this.canvas.width = width;
    this.canvas.height = height;
  
    return setInterval(() => {
      this.ctx.drawImage(this.video, 0, 0, width, height);
      const pixels = this.ctx.getImageData(0, 0, width, height);
      this.ctx.putImageData(this.greenScreen(pixels), 0, 0);
    }, 20);
  }

  paintGreenToCanavas() {
    const width = this.video.videoWidth;
    const height = this.video.videoHeight;
    this.canvas.width = width;
    this.canvas.height = height;
  
    return setInterval(() => {
      this.ctx.drawImage(this.video, 0, 0, width, height);
      const pixels = this.ctx.getImageData(0, 0, width, height);
      this.ctx.putImageData(this.rgbColorSplit(pixels), 0, 0);
    }, 20);
  }

  paintRedToCanavas() {
    const width = this.video.videoWidth;
    const height = this.video.videoHeight;
    this.canvas.width = width;
    this.canvas.height = height;
  
    return setInterval(() => {
      this.ctx.drawImage(this.video, 0, 0, width, height);
      const pixels = this.ctx.getImageData(0, 0, width, height);
      this.ctx.putImageData(this.redEffect(pixels), 0, 0);
    }, 20);
  }
  
  takePhoto() {
    this.audioSnap.currentTime = 0;
    this.audioSnap.play();
  
    const data = this.canvas.toDataURL('image/jpeg');
    const link = document.createElement('a');
    link.href = data;
    link.setAttribute('download', 'photo');
    link.textContent = 'Download Image';
    link.innerHTML = `<img src="${data}" alt="photo"/>`;
    this.strip.insertBefore(link, this.strip.firstChild);
  }
  
  redEffect(pixels) {
    const renderPixels = pixels;
    for (let i = 0; i < renderPixels.data.length; i += 4) {
      renderPixels.data[i + 0] = renderPixels.data[i + 0] + 100;
      renderPixels.data[i + 1] = renderPixels.data[i + 1] - 50;
      renderPixels.data[i + 2] = renderPixels.data[i + 2] * 0.5;
    }
    return renderPixels;
  };
  
  rgbColorSplit(pixels){
    const renderPixels = pixels;
    for (let i = 0; i < renderPixels.data.length; i += 4) {
      renderPixels.data[i - 150] = renderPixels.data[i + 0];
      renderPixels.data[i + 500] = renderPixels.data[i + 1];
      renderPixels.data[i - 550] = renderPixels.data[i + 2];
    }
    return renderPixels;
  };
  
  greenScreen(pixels) {
    const levels = {}; 
  
    document.querySelectorAll('.rgb input').forEach((input) => {
      levels[input.name] = input.value;
    });
  
    for (let i = 0; i < pixels.data.length; i = i + 4) {
      let red = pixels.data[i + 0];
      let green = pixels.data[i + 1];
      let blue = pixels.data[i + 2];
  
      if (red >= levels.rmin
        && green >= levels.gmin
        && blue >= levels.bmin
        && red <= levels.rmax
        && green <= levels.gmax
        && blue <= levels.bmax) {
        pixels.data[i + 3] = 0;
      }
    }
  
    return pixels;
  };
  
  addListeners() {
    this.video.addEventListener('canplay', this.paintToCanavas.bind(this));
    this.photoBtn.addEventListener('click', this.takePhoto.bind(this));
    this.greenBtn.addEventListener('click', this.paintGreenToCanavas.bind(this));
    this.redBtn.addEventListener('click', this.paintRedToCanavas.bind(this));

  };

}

new UnrealVideo();
