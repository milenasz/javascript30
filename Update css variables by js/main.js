class ScopedVariables {

    constructor() {
        this.inputs = document.querySelectorAll('.controls input');
        this.addListenerChange();
        this.addListenerMousemove();
    }

    handleUpdate() {
        const suffix = this.dataset.sizing || '';
        document.documentElement.style.setProperty(`--${this.name}`, this.value + suffix);
    }

    addListenerChange() {
        this.inputs.forEach(input => input.addEventListener('change', this.handleUpdate));
    }

    addListenerMousemove() {
        this.inputs.forEach(input => input.addEventListener('mousemove', this.handleUpdate));

    }
    
}

new ScopedVariables();

