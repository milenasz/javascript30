class VideoSpeedController {
  constructor() {
    this.speed = document.querySelector('.speed');
    this.bar = this.speed.querySelector('.speed-bar');
    this.video = document.querySelector('.flex');
    this.click = false;

    this.addListeners();
  }

  updateBar(height, playbackRate) {
    this.bar.style.height = height;
    this.bar.textContent = playbackRate.toFixed(2) + 'x';
    this.video.playbackRate = playbackRate;
  }

  setValues(e) {
    const y = e.pageY - this.speed.offsetTop;
    const percent = y / this.speed.offsetHeight;
    const min = 0.4;
    const max = 4;
    const height = Math.round(percent * 100) + '%';
    const playbackRate = percent * (max - min) + min;

    this.updateBar(height, playbackRate)
  }

  handleMove(e) {
    this.click && this.setValues(e);
  }

  addListeners() {
    this.speed.addEventListener('mousedown', () => this.click = true);
    this.speed.addEventListener('mousemove', (e) => this.handleMove(e));
    this.speed.addEventListener('mouseup', () => this.click = false);
  }

}

new VideoSpeedController();
